//
//  ViewController.m
//  MXImageSelectView
//
//  Created by maxin on 16/4/27.
//  Copyright © 2016年 maxin. All rights reserved.
//

#import "ViewController.h"
#import "LCReleaseContentViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    LCReleaseContentViewController *releaseContentViewController = [[LCReleaseContentViewController alloc] init];
    [self.navigationController pushViewController:releaseContentViewController animated:YES];
}


@end
