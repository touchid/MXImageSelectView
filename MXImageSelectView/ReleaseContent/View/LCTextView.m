//
//  ViewController.m
//  UI022702TextField
//
//  Created by admin on 2017/2/27.
//  Copyright © 2017年 LC. All rights reserved.
//

#import "LCTextView.h"
#import <Masonry.h>

@interface LCTextView ()<UITextViewDelegate>

@property (weak, nonatomic) UITextView *textField;

@property (weak, nonatomic) UILabel *textLabel;

@end

@implementation LCTextView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}

-(void)textFiledEditChanged{
    
    //检测文本改变
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(putInTextFieldEditChanged:) name:@"UITextFieldTextDidChangeNotification" object:self.textField];
    
}

#define kMaxLength 5

-(void)putInTextFieldEditChanged:(NSNotification *)obj {
    
    UITextField *textField = (UITextField *)obj.object;
    
    NSString *toBeString = textField.text;
    #pragma mark -下午8:17 字数限制 -
    NSInteger index = kMaxLength - toBeString.length;
   
    // 键盘输入模式
    
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage;
    
    if ([lang isEqualToString:@"zh-Hans"]) { // 简体中文输入，包括简体拼音，健体五笔，简体手写
        
        UITextRange *selectedRange = [textField markedTextRange];
        
        //获取高亮部分
        
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        
        if (!position) {
            
            if (toBeString.length > kMaxLength) {
                
                textField.text = [toBeString substringToIndex:kMaxLength];
                
            }
            
        }
        
        // 有高亮选择的字符串，则暂不对文字进行统计和限制
        
        else{
            
        }
        
    }
    
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    
    else{
        if (toBeString.length > kMaxLength) {
            
            textField.text = [toBeString substringToIndex:kMaxLength];
            
        }
    }
                #pragma mark - 下午8:16 字数限制 -
    if (index < 0) {
        index = 0;
    
    }
    self.textLabel.textColor = [UIColor redColor];
    self.textLabel.text = [NSString stringWithFormat:@"%ld字以内", (long)index];

 
}

- (void)setupUI {

    UITextView *textField = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width -20, 300)];
    NSLog(@"textField = %@", textField);
    _textField = textField;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(putInTextFieldEditChanged:) name:UITextViewTextDidChangeNotification object:textField];
//    textField.placeHolderLabel.text = @"描述......";
    [self addSubview:textField];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(10);
        make.left.offset(10);
        make.right.offset(-10);
        make.height.offset(300);
    }];

    

    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 500, 40)];
    _textLabel = textLabel;
    textLabel.font = [UIFont systemFontOfSize:13];
    textLabel.textColor = [UIColor lightGrayColor];
    textLabel.text = @"描述......";
    [self addSubview:textLabel];
    [textLabel sizeToFit];
    [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(textField.mas_left).offset(3);
        make.top.equalTo(textField.mas_top).offset(6);
    }];
    NSLog(@"textLabel = %@", textLabel);

}



@end
