//
//  LCReleaseContentViewController.m
//  MXImageSelectView
//
//  Created by admin on 2017/10/6.
//  Copyright © 2017年 admin. All rights reserved.
//

#import "LCReleaseContentViewController.h"
#import <Masonry.h>
#import <MBProgressHUD.h>
#import "LCTextView.h"
#import "MXPhotoView.h"

@interface LCReleaseContentViewController ()<UITextViewDelegate,MBProgressHUDDelegate>
@property (nonatomic, weak) UIView *contentView;
@property (nonatomic, weak) UILabel *placeHolderLabel;
@property (nonatomic, assign) NSInteger strCount;
@property (nonatomic, strong) MBProgressHUD *HUD;

@end

@implementation LCReleaseContentViewController{ 
    LCTextView *_textView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTextView];
    [self setupPhotoView];
    self.navigationController.navigationBar.translucent = NO;

    self.edgesForExtendedLayout = UIRectEdgeNone;

}

#pragma mark -
#pragma mark 设置TextView
-(void)setupPhotoView{
    MXPhotoView *photoView = [[MXPhotoView alloc] init];
    photoView.photoViewDele = self;
    photoView.isNeedMovie = YES;
    photoView.showNum = 4;
    [self.view addSubview:photoView];
    
    [photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_textView.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.offset(300);
    }];
}
-(void)setupTextView{
    
    LCTextView *textView = [[LCTextView alloc] init];
    _textView = textView;
    [self.view addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(10);
        make.left.offset(10);
        make.right.offset(-10);
        make.height.offset(100);
    }];

 //    self.automaticallyAdjustsScrollViewInsets = NO;
  
}

@end
